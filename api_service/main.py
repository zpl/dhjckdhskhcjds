# -*- coding: utf-8 -*-

import os
import sys
from aiohttp import web
import asyncio

# add ../ directory to search path.
# to be able run globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from api_service.fetcher import Fetcher


def safe_cast(val, default):
    """
    Safe cast value to int
    :param val:
    :return:
    """
    try:
        return int(val)
    except (ValueError, TypeError):
        return default


async def index(request):
    limit = request.rel_url.query.get('limit', 200)

    return web.json_response(await request.app.fetcher.get_ranking(safe_cast(limit, 200)))


async def make_app():
    app = web.Application()
    app.fetcher = Fetcher()
    while True:
        if await app.fetcher.connect(
            host=os.environ.get('MYSQL_HOST'),
            port=os.environ.get('MYSQL_PORT'),
            user=os.environ.get('MYSQL_USER'),
            password=os.environ.get('MYSQL_PASSWORD'),
            db=os.environ.get('MYSQL_DATABASE')
        ):
            break

        await asyncio.sleep(5)


    app.add_routes([web.get('/', index)])
    return app


web.run_app(make_app())
