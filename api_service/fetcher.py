# -*- coding: utf-8 -*-
import logging
import aiomysql


class Fetcher:
    def __init__(self):
        self._pool = None

    async def connect(self, host, port, user, password, db):
        """
        connect fetcher to database
        :param host:
        :param port:
        :param user:
        :param password:
        :param db:
        :return:
        """
        try:
            self._pool = await aiomysql.create_pool(host=host, port=int(port), user=user, password=password, db=db)
            return True
        except Exception as e:
            logging.exception(e)

        return False

    async def get_ranking(self, limit=200):
        """
        get ranking stats from database
        :param limit:
        :return:
        """
        async with self._pool.acquire() as conn:
            async with conn.cursor() as cur:
                # "we strongly recommend that you don't use any application frameworks.
                # We want to clearly see and understand your own style and intentions without being influenced by any conventions imposed by a framework."
                # It's better to use ORM here.

                stmt = """
                    SELECT cr.id, cr.coin_name, TRIM(cp.price)+0 as price
                    FROM coins_rank AS cr
                           LEFT JOIN coins_pricing cp on cp.coin_name = cr.coin_name
                    ORDER BY cr.id LIMIT %s
                """
                await cur.execute(stmt, limit)
                return await cur.fetchall()
