# -*- coding: utf-8 -*-

import os
import sys

# add ../ directory to search path.
# to be able run tests globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

import aiounittest
from aioresponses import aioresponses

from asynctest import patch, CoroutineMock

from pricing_service.coinmarketcap import CoinmarketcapFetcher

COINMARKETCAP_URL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=5000&convert=USD"


class TestCoinmarketcap(aiounittest.AsyncTestCase):

    def setup_mock(self, mock, url, filename, status=200, exception=None):
        """
        set up mock given url

        :param mock: mock object
        :param url: url for the mock
        :param filename: filename of fixture
        :param status: http status
        :param exception: exception to raise
        :return:
        """
        with open(filename, 'r') as myfile:
            body = myfile.read().replace('\n', '')

        mock.get(url, status=status, body=body, exception=exception)

    def setUp(self):
        self._coinmarketcap = CoinmarketcapFetcher({'X-CMC_PRO_API_KEY': 'aabbcc'})

    @patch("aiohttp.ClientSession.get", CoroutineMock())
    @aioresponses()
    async def test_set_header(self, mock):
        """
        testing setting authorization header
        :param mock:
        :return:
        """
        self.setup_mock(mock, COINMARKETCAP_URL, 'pricing_service/mocks/valid_response.txt')

        async with self._coinmarketcap:
            data = await self._coinmarketcap.fetch_prices()

            self._coinmarketcap._session.get.assert_called_once_with(COINMARKETCAP_URL,
                                                                     headers={"X-CMC_PRO_API_KEY": "aabbcc"})

    @aioresponses()
    async def test_set_fetch_prices(self, mock):
        """
        testing fetch prices list
        :param mock:
        :return:
        """
        self.setup_mock(mock, COINMARKETCAP_URL, 'pricing_service/mocks/valid_response.txt')

        async with self._coinmarketcap:
            data = await self._coinmarketcap.fetch_prices()

        assert data[0].coin_name == 'BTC'
        assert data[0].price == 3871.38015045

        assert data[-1].coin_name == 'CGEN'
        assert data[-1].price == 0.0000362335736709
