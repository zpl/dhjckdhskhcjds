# -*- coding: utf-8 -*-

from libs.fetcher import Fetcher
from collections import namedtuple

COINMARKETCAP_URL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?start=1&limit=5000&convert=USD"


class CoinmarketcapFetcher(Fetcher):
    Result = namedtuple('CoinmarketcapResult', ['coin_name', 'price'])

    async def fetch_prices(self):
        """
        fetches price list from coinmarketcap and generate namedtuple for each coin stat
        :return:
        """
        result = await self.fetch_json(COINMARKETCAP_URL)
        if result:
            return list(
                map(lambda x: CoinmarketcapFetcher.Result(x['symbol'], x['quote']['USD']['price']), result['data'])
            )

        return None
