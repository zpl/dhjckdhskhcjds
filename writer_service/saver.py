# -*- coding: utf-8 -*-

import aiomysql
import logging


class Saver:
    def __init__(self, host, port, user, password, db):
        self._host = host
        self._port = port
        self._user = user
        self._password = password
        self._db = db
        self._pool = None

    async def connect(self):
        """
        connect saver to database
        :return:
        """
        try:
            self._pool = await aiomysql.create_pool(
                host=self._host,
                port=int(self._port),
                user=self._user,
                password=self._password,
                db=self._db
            )
        except Exception as e:
            logging.exception(e)
            return False

        return True

    async def save_ranking(self, data):
        """
        update renking information
        :param data:
        :return:
        """
        async with self._pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute("TRUNCATE coins_rank")

                stmt = 'INSERT IGNORE INTO coins_rank (coin_name) VALUES (%s)'
                await cur.executemany(stmt, data)

                await cur.execute('COMMIT;')

    async def save_pricing(self, data):
        """
        update pricing information
        :param data:
        :return:
        """
        async with self._pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute("TRUNCATE coins_pricing")

                stmt = 'INSERT IGNORE INTO coins_pricing (coin_name, price) VALUES (%s, %s)'
                await cur.executemany(stmt, data)

                await cur.execute('COMMIT;')
