# -*- coding: utf-8 -*-

import os
import sys

import logging
import asyncio

# add ../ directory to search path.
# to be able run globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from libs.queue import Queue
from writer_service.saver import Saver


async def main():
    while True:
        await asyncio.sleep(1)
        try:
            saver = Saver(
                host=os.environ.get('MYSQL_HOST'),
                port=os.environ.get('MYSQL_PORT'),
                user=os.environ.get('MYSQL_USER'),
                password=os.environ.get('MYSQL_PASSWORD'),
                db=os.environ.get('MYSQL_DATABASE')
            )
            if await saver.connect():
                queue = Queue(
                    host=os.environ.get('RABBITMQ_HOST'),
                    user=os.environ.get('RABBITMQ_USER'),
                    password=os.environ.get('RABBITMQ_PASSWORD')
                )
                if await queue.connect() and await queue.open_channel():

                    ranking_queue = await queue.declare_queue('ranking_queue')
                    pricing_queue = await queue.declare_queue('pricing_queue')

                    await asyncio.wait(
                        [
                            queue.consume(ranking_queue, saver.save_ranking),
                            queue.consume(pricing_queue, saver.save_pricing)
                        ],
                        return_when=asyncio.FIRST_COMPLETED)

        except Exception as e:
            logging.error(e)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
