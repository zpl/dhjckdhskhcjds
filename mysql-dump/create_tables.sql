CREATE TABLE `coins_rank`
(
  `id`        INT unsigned NOT NULL AUTO_INCREMENT,
  `coin_name` VARCHAR(10),
  PRIMARY KEY (`id`)
) ENGINE = InnoDB;



CREATE TABLE `coins_pricing`
(
  `coin_name` VARCHAR(10),
  `price`     DECIMAL(40, 30),
  PRIMARY KEY (`coin_name`)
) ENGINE = InnoDB;