# Top Coins

The solution for [WATTx Software Engineer Challenge: Top Coins](https://github.com/WATTx/code-challenges/blob/master/software-engineer-challenge-top-coins.md)

By **[Denis Shirokov](dan@zlogic.ru)**

## TL;DR;

```
cp env.default .env
# edit .env
docker-compose up

# and navigate to http://localhost:8080/

```

I expect there is [autoenv](https://github.com/kennethreitz/autoenv) installed, if not source .env will work


## Architecture

The key idea is to keep each service isolated. That's why I decided to use RabbitMQ as the System bus.


![Architecture](architecture.png)

### Database
I decided to use the database to keep the results.

There is possibility work without a database at all (Keep most recent data in memory at API service)

However, I couldn't find any advantages to that. 
It will increase the amount of code base, so it will be harder to maintain.
Also, it will require some time after restarting service, to fetch new data.


if we expect very high RPS at API service, we can set up memcache before it.
Also in current architecture, we can scale API service horizontally. (run many instances).


### Structure
![Database](database.png)

**coins_rank** contains rank from ranking service

**coins_pricing** contains pricing for each coin from pricing service


To get the list, we will do a join. LEFT or INNER. Depends, what we want to get.

For example, if we don't have the price for coin in coins rank. Should we display it at API service without price, or we should exclude it?

With this schema, we can use not only crypto compare for rank. We can add additional services. Just by adding another list tables.

#### Performance
Joining by primary key is very fast. It's comparable to just selecting by primary key.

However, if the list will be more significant than 200 items or queries will be very frequent - we can add cache as I mentioned before, or we can make temp table (materialized view)


#### Example query

```sql
SELECT ca.coin_name, 
       cr.price 
FROM   coins_rank ca 
       LEFT JOIN coins_pricing cp 
               ON ca.coin_name = co.coin_name 
ORDER  BY cr.id 
```

## Match problem

I assumed that coin short name (like BTC) should be unique. However, this is not right.
This should be checked from the business side, why this also happens, how to solve it (from a business perspective)

from a technical perspective - we should change coin_name in both tables to coin_id

So introduce coins table. After that by some business logic we should match coin from some service to our internal id. 


## Services

### Pricing service
Fetched pricing data from **coinmarketcap** and in case of success sends it data to RabbitMQ

### Ranking service
Fetched rank data from **crypto compare** and in case of success sends it data to RabbitMQ

Fetching rank information for top 200 coins from crypto compare requires 2 calls to API, and it's important to make sure that both calls completed successfully before we can use data.

Otherwise, the rank list might be broken.

**assumption:**

crypto compare provides different TOP lists, and I picked "Toplist by Market Cap Full Data."

In the real system, I would ask first what's the list if needed.
However, anyway, it's straightforward to change.

### Writer service

Reads data from RabbitMQ and updates database


Current architecture gives up ability partial data update.
We can safely update rank information and pricing information at a different time.

However, there might be a situation in the future, when data will be related.

that's why writer service might be another advantage - it might keep data for service A and wait for data from service B before updating the database.



### API service

Reads data from the database and makes it accessible via the web.

In the case of high load, it's better to add caching service (Memcache) before it



## Running the tests

to run unit tests execute following commands from top of this repo


```
pip install -r requirements.txt
python -m unittest -v
```

