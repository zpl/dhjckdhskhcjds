# -*- coding: utf-8 -*-

import os
import sys

from asynctest import patch, CoroutineMock

# add ../ directory to search path.
# to be able run tests globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from aiohttp.client_exceptions import ClientConnectionError
import aiounittest
from aioresponses import aioresponses

from libs.fetcher import Fetcher

EXAMPLE_URL = 'http://example.com'


class TestFetcher(aiounittest.AsyncTestCase):

    def setup_mock(self, mock, url, filename, status=200, exception=None):
        """
        set up mock given url

        :param mock: mock object
        :param url: url for the mock
        :param filename: filename of fixture
        :param status: http status
        :param exception: exception to raise
        :return:
        """
        with open(filename, 'r') as myfile:
            body = myfile.read().replace('\n', '')

        mock.get(url, status=status, body=body, exception=exception)

    def setUp(self):
        self._fetcher = Fetcher()

    @aioresponses()
    async def test_fetch_json_success(self, mock):
        """
        Testing successful case of fetch_json method
        :param mock:
        :return:
        """
        self.setup_mock(mock, EXAMPLE_URL, 'libs/tests/mocks/valid_json.txt')

        async with self._fetcher:
            data = await self._fetcher.fetch_json(EXAMPLE_URL)

        assert data[0]['first_name'] == 'Lores'

    @aioresponses()
    async def test_fetch_json_404(self, mock):
        """
        testing fetch_json on 404 url
        :param mock:
        :return:
        """
        self.setup_mock(mock, EXAMPLE_URL, 'libs/tests/mocks/valid_json.txt', status=404)

        async with self._fetcher:
            data = await self._fetcher.fetch_json(EXAMPLE_URL)

        assert data is None

    @aioresponses()
    async def test_fetch_json_error(self, mock):
        """
        testing fetching_json on ClientConnectionErrors
        :param mock:
        :return:
        """
        self.setup_mock(mock, EXAMPLE_URL, 'libs/tests/mocks/valid_json.txt',
                        exception=ClientConnectionError())

        async with self._fetcher:
            data = await self._fetcher.fetch_json(EXAMPLE_URL)

        assert data is None

    @aioresponses()
    async def test_fetch_json_error(self, mock):
        """
        testing fetching_json with broken json
        :param mock:
        :return:
        """
        self.setup_mock(mock, EXAMPLE_URL, 'libs/tests/mocks/broken_json.txt')

        async with self._fetcher:
            data = await self._fetcher.fetch_json(EXAMPLE_URL)

        assert data is None

    @patch("aiohttp.ClientSession.get", CoroutineMock())
    @aioresponses()
    async def test_set_header(self, mock):
        self._fetcher = Fetcher(headers={"Authorization": "Basic somekeyhere"})

        self.setup_mock(mock, EXAMPLE_URL, 'libs/tests/mocks/valid_json.txt')

        async with self._fetcher:
            data = await self._fetcher.fetch_json(EXAMPLE_URL)
            self._fetcher._session.get.assert_called_once_with(EXAMPLE_URL,
                                                               headers={"Authorization": "Basic somekeyhere"})
