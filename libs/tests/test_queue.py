# -*- coding: utf-8 -*-

import os
import sys

import asynctest
import logging

# add ../ directory to search path.
# to be able run tests globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from libs.queue import Queue


async def async_return_coro():
    return asynctest.CoroutineMock()


asynctest.MagicMock.__await__ = lambda x: async_return_coro().__await__()


class TestFetcher(asynctest.TestCase):
    def setUp(self):
        self._queue = Queue(user='guest', password='guest', host='127.0.0.1')

    @asynctest.patch('aio_pika.connect_robust')
    async def test_queue_connect(self, mock):
        """
        Successful connect case
        :param mock:
        :return:
        """
        mock.return_value = asynctest.CoroutineMock()

        await self._queue.connect()

        assert self._queue._connection

        mock.assert_called_once_with('amqp://guest:guest@127.0.0.1/')

    @asynctest.patch('aio_pika.connect_robust')
    async def test_queue_connect_error(self, mock):
        """
        Connection error
        :param mock:
        :return:
        """
        mock.side_effect = ValueError('some error')
        await self._queue.connect()

        assert not self._queue._connection

    @asynctest.patch('aio_pika.connect_robust')
    async def test_open_channel(self, mock):
        """
        Test open channel
        :param mock:
        :return:
        """

        mock.return_value = asynctest.CoroutineMock()

        await self._queue.connect()
        await self._queue.open_channel()

        mock.return_value.channel.assert_called_with()

        assert self._queue._connection
        assert self._queue._channel
