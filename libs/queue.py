# -*- coding: utf-8 -*-

import logging
import json
import aio_pika


class Queue:
    def __init__(self, host, user, password):
        self._connection = None
        self._channel = None
        self._host = host
        self._user = user
        self._password = password

    def _get_url(self):
        """
        build and return connection url
        :return:
        """
        return "amqp://{}:{}@{}/".format(self._user, self._password, self._host)

    async def connect(self):
        """
        connect to server
        :return:
        """
        try:
            self._connection = await aio_pika.connect_robust(self._get_url())
            return True

        except Exception as e:
            logging.error(e)

        return False

    async def open_channel(self):
        """
        open connection channel
        :return:
        """
        if not self._connection:
            return False

        try:
            self._channel = await self._connection.channel()
            return True
        except Exception as e:
            logging.error(e)

        return False

    async def declare_queue(self, queue_name):
        """
        declare queue
        :return:
        """
        try:
            return await self._channel.declare_queue(
                queue_name,
                auto_delete=True
            )
        except Exception as e:
            logging.debug(e)

        return None

    async def consume(self, queue, handler):
        """
        consume messages from queue
        :param queue:
        :param handler:
        :return:
        """
        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    await handler(json.loads(message.body))

    async def publish(self, queue_name, data):
        """
        publish message to queue
        :param queue_name:
        :param data:
        :return:
        """
        if not self._channel:
            return

        await self._channel.default_exchange.publish(
            aio_pika.Message(
                body=json.dumps(data).encode()
            ),
            routing_key=queue_name
        )
