# -*- coding: utf-8 -*-

import logging
import aiohttp


class Fetcher:
    def __init__(self, headers=None):
        """
        init fetcher. optional headers can be passed to be used in each query
        :param headers:
        """

        self._headers = {}

        if headers:
            self._headers = headers

    async def fetch_json(self, url):
        """
        Fetches json from given url
        :param url:
        :return: parsed json data
        """
        if not self._session:
            return None

        try:
            async with self._session.get(url, headers=self._headers) as r:
                if r.status == 200:
                    return await r.json()
        except Exception as e:
            logging.debug(e)

        return None

    async def __aenter__(self):
        """
        asynchronous context manager
        create session at async with ...
        :return:
        """

        try:
            self._session = aiohttp.ClientSession()
        except Exception as e:
            logging.debug(e)

    async def __aexit__(self, type, value, traceback):
        """
        asynchronous context manager
        close session at exit
        :param type:
        :param value:
        :param traceback:
        :return:
        """
        try:
            await self._session.close()
        except Exception as e:
            logging.debug(e)
        self._session = None
