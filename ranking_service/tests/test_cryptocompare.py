# -*- coding: utf-8 -*-

import os
import sys

# add ../ directory to search path.
# to be able run tests globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from aiohttp.client_exceptions import ClientConnectionError
import aiounittest
from aioresponses import aioresponses

from ranking_service.cryptocompare import CryptocompareFetcher

CRYPTOCOMPARE_PAGE_1_URL = "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=100&tsym=USD&page=0"
CRYPTOCOMPARE_PAGE_2_URL = "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=100&tsym=USD&page=1"


class TestCryptocompare(aiounittest.AsyncTestCase):

    def setup_mock(self, mock, url, filename, status=200, exception=None):
        """
        set up mock given url

        :param mock: mock object
        :param url: url for the mock
        :param filename: filename of fixture
        :param status: http status
        :param exception: exception to raise
        :return:
        """
        with open(filename, 'r') as myfile:
            body = myfile.read().replace('\n', '')

        mock.get(url, status=status, body=body, exception=exception)

    def setUp(self):
        self._cryptocompare = CryptocompareFetcher()

    @aioresponses()
    async def test_fetch_top_list_one_page(self, mock):
        """
        testing fetch_top_list for given page
        :param mock:
        :return:
        """
        self.setup_mock(mock, CRYPTOCOMPARE_PAGE_1_URL, 'ranking_service/mocks/success_page1.txt')

        async with self._cryptocompare:
            currency = await self._cryptocompare.fetch_top_list(page=0)

        assert len(currency) == 100
        assert currency[0] == 'BTC'
        assert currency[-1] == 'PAY'

    @aioresponses()
    async def test_fetch_top_list_all(self, mock):
        """
        testing fetch both pages

        :param mock:
        :return:
        """

        self.setup_mock(mock, CRYPTOCOMPARE_PAGE_1_URL, 'ranking_service/mocks/success_page1.txt')
        self.setup_mock(mock, CRYPTOCOMPARE_PAGE_2_URL, 'ranking_service/mocks/success_page2.txt')

        async with self._cryptocompare:
            currency = await self._cryptocompare.fetch_full_list()

        assert len(currency) == 200
        assert currency[0] == 'BTC'
        assert currency[99] == 'PAY'
        assert currency[-1] == 'SLS'

    @aioresponses()
    async def test_fetch_top_list_all_one_faulty(self, mock):
        """
        testing fetch both pages

        one page faulty


        :param mock:
        :return:
        """

        self.setup_mock(mock, CRYPTOCOMPARE_PAGE_1_URL, 'ranking_service/mocks/success_page1.txt')
        self.setup_mock(mock, CRYPTOCOMPARE_PAGE_2_URL, 'ranking_service/mocks/success_page2.txt',
                        exception=ClientConnectionError())

        async with self._cryptocompare:
            currency = await self._cryptocompare.fetch_full_list()

        assert currency is None
