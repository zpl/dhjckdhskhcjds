# -*- coding: utf-8 -*-

from libs.fetcher import Fetcher

CRYPTOCOMPARE_URL = "https://min-api.cryptocompare.com/data/top/mktcapfull?limit=100&tsym=USD&page="


class CryptocompareFetcher(Fetcher):
    async def fetch_top_list(self, page=0):
        """
        return list list of currencies from cryptocompare for given page
        :param page:
        :return:
        """
        result = await self.fetch_json(CRYPTOCOMPARE_URL + str(page))

        if result:
            return list(map(lambda x: x['CoinInfo']['Name'], result['Data']))

        return None

    async def fetch_full_list(self):
        """
        fetch full list

        NOTE: we don't need result, if one of the page fetched incorrectly
        :return:
        """

        result = []

        for page in (0, 1):
            data = await self.fetch_top_list(page)
            if not data:
                return None

            result += data

        return result
