# -*- coding: utf-8 -*-

import os
import sys

import logging
import asyncio

# add ../ directory to search path.
# to be able run globally
sys.path.append(os.path.abspath(os.path.dirname(__file__) + "/.."))

from libs.queue import Queue
from ranking_service.cryptocompare import CryptocompareFetcher


async def main():
    while True:
        await asyncio.sleep(1)
        queue = Queue(
            host=os.environ.get('RABBITMQ_HOST'),
            user=os.environ.get('RABBITMQ_USER'),
            password=os.environ.get('RABBITMQ_PASSWORD')
        )
        if await queue.connect() and await queue.open_channel():

            while True:
                try:
                    fetcher = CryptocompareFetcher()
                    async with fetcher:
                        data = await fetcher.fetch_full_list()
                        if data:
                            logging.error('updated')
                            await queue.publish('ranking_queue', data)
                        await asyncio.sleep(120)
                except Exception as e:
                    logging.error(e)
                    await asyncio.sleep(10)
                    break


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()
